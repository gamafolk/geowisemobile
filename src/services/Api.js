import axios from 'axios';

export default class Api {
	static conn = undefined;

	static async setHost(host) {
		this.conn = axios.create({
			baseURL: host,
		});
	}
}
