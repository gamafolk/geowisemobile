import Realm from 'realm';
import AsyncStorage from '@react-native-community/async-storage';
import { version } from '../schemas/schemas.json';

export default class Db {
	static realm = undefined;

	static async open() {
		const schema = await AsyncStorage.getItem('schema');

		this.realm = await Realm.open({
			schema: JSON.parse(schema),
			schemaVersion: version,
		});
	}
}
