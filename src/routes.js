/* eslint-disable class-methods-use-this */
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer as Container } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Storage from '@react-native-community/async-storage';

import Icon from 'react-native-vector-icons/MaterialIcons';

import Api from './services/Api';
import Db from './services/Realm';

import MenuLogo from './Components/MenuLogo';
import headerDefaults from './config/HeaderDefaults';

import Welcome from './pages/Welcome';
import Login from './pages/Login';
import Territoriais from './pages/Territoriais';
import Avaliacoes from './pages/Avaliacoes';
import Autonomas from './pages/Autonomas';
import Map from './pages/Map';
import Synchronization from './pages/Synchronization';
import Settings from './pages/Settings';
import Edition from './pages/Edition';

const {
	Navigator: TabNavigator,
	Screen: TabScreen,
} = createBottomTabNavigator();

const {
	Navigator: StackNavigator,
	Screen: StackScreen,
} = createStackNavigator();

export default class Routes extends Component {
	state = { host: '', show: false };

	async componentDidMount() {
		// Provisório
		// await Storage.setItem('logged-user', '');

		const host = await Storage.getItem('host');

		if (host) {
			Api.setHost(host);
			await Db.open();
		}

		this.setState({ host: host || '', show: true });
	}

	render() {
		const { host, show } = this.state;

		const mainSubRoutes = () => (
			<StackNavigator>
				<StackScreen
					name="Territoriais"
					component={Territoriais}
					options={headerDefaults}
				/>
				<StackScreen
					name="Autonomas"
					component={Autonomas}
					options={headerDefaults}
				/>
				<StackScreen
					name="Avaliacoes"
					component={Avaliacoes}
					options={headerDefaults}
				/>
				<StackScreen
					name="Edition"
					component={Edition}
					options={headerDefaults}
				/>
			</StackNavigator>
		);

		const mainRoutes = () => {
			const settingRoutes = () => (
				<StackNavigator>
					<StackScreen
						name="Settings-stack"
						component={Settings}
						options={headerDefaults}
					/>
				</StackNavigator>
			);

			const syncRoutes = () => (
				<StackNavigator>
					<StackScreen
						name="Synchronization-stack"
						component={Synchronization}
						options={headerDefaults}
					/>
				</StackNavigator>
			);

			const mapRoutes = () => (
				<StackNavigator>
					<StackScreen
						name="Map-stack"
						component={Map}
						options={headerDefaults}
					/>
				</StackNavigator>
			);

			return (
				<TabNavigator
					tabBarOptions={{
						showLabel: false,
						activeBackgroundColor: '#003967',
						inactiveBackgroundColor: '#003967',
						style: { height: 40 },
						keyboardHidesTabBar: true,
					}}
				>
					<TabScreen
						name="Registers"
						component={mainSubRoutes}
						options={{
							tabBarIcon: ({ focused }) => {
								return (
									<Icon
										name="home"
										size={25}
										color={focused ? '#a3d6ff' : '#eee'}
									/>
								);
							},
						}}
					/>
					<TabScreen
						name="Map"
						component={mapRoutes}
						options={{
							tabBarIcon: ({ focused }) => {
								return (
									<Icon
										name="map"
										size={25}
										color={focused ? '#a3d6ff' : '#eee'}
									/>
								);
							},
						}}
					/>
					<TabScreen
						name="Sync"
						component={syncRoutes}
						options={{
							title: 'Sincronização',
							tabBarIcon: ({ focused }) => {
								return (
									<Icon
										name="sync"
										size={25}
										color={focused ? '#a3d6ff' : '#eee'}
									/>
								);
							},
						}}
					/>
					<TabScreen
						name="Settings"
						component={settingRoutes}
						options={{
							tabBarIcon: ({ focused }) => {
								return <MenuLogo focused={focused} />;
							},
						}}
					/>
				</TabNavigator>
			);
		};

		return (
			<>
				<Container>
					{show && (
						<TabNavigator>
							{!host.trim() && (
								<TabScreen
									name="Welcome"
									component={Welcome}
									options={{
										title: 'Bem vindo!',
										tabBarVisible: false,
									}}
								/>
							)}
							<TabScreen
								name="Login"
								component={Login}
								options={{
									tabBarVisible: false,
								}}
							/>
							<TabScreen
								name="MainRoutes"
								component={mainRoutes}
								options={{
									tabBarVisible: false,
								}}
							/>
						</TabNavigator>
					)}
				</Container>
			</>
		);
	}
}
