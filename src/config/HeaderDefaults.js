const defaults = {
	headerStyle: {
		height: 36,
	},
	headerTitleStyle: {
		fontSize: 15,
		color: '#003967',
	},
	headerTitleAlign: 'center',
};

export default defaults;
