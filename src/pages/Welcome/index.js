/* eslint-disable class-methods-use-this */
/* eslint-disable no-plusplus */
import React, { Component } from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Text, Form, Input } from './styles';

import Api from '../../services/Api';
import Db from '../../services/Realm';
import dbTypes from '../../config/dataBaseTypes.json';
import schemaData from '../../schemas/schemas.json';

import Button from '../../Components/Button';

import {
	getAttrs,
	confirmConnection,
	getTypes,
	getUsers
} from '../../config/apiActions.json';

import { deviceName } from '../../config/deviceName.json'; // Provisório

export default class Login extends Component {
	state = { host: 'http://192.168.2.31:63005/coleta/ws/', finished: false };

	async handleConect() {
		const { host } = this.state;
		const testApi = axios.create({
			baseURL: host.trim(),
		});

		try {
			await testApi.get(confirmConnection);
			await AsyncStorage.setItem('host', host.trim());
			Api.setHost(host.trim());
			await this.generateSchemas();
		} catch (error) {
			tron.error(error);
			// Mostrar uma mensagem de erro
		}
	}

	async generateSchemas() {
		try {
			const { data } = await Api.conn.get(getAttrs);
			const newSchemas = [];
			data.forEach((attr) => {
				const schema = newSchemas.filter(
					(e) => e.name === attr.table
				)[0];
				if (!schema) {
					newSchemas.push({
						name: attr.table,
						primaryKey: 'id',
						properties: {
							id: { type: 'int', indexed: true },
						},
					});
				} else {
					schema.properties[attr.column] = {
						type: dbTypes[attr.tipo_atributo],
						optional: true,
					};
				}
			});

			const schemas = [...schemaData.models, ...newSchemas];

			// Salvando as tabelas para serem criadas no banco dedados
			await AsyncStorage.setItem('schema', JSON.stringify(schemas));

			// Iniciando o banco de dados, criando tabelas
			await Db.open();

			// Populando tabelas principais
			await this.populateTypes();
			await this.populateAttributes();
			await this.populateUsers();

			this.setState({ finished: true });
		} catch (error) {
			tron.error(error);
		}
	}

	async populateTypes() {
		try {
			const { data } = await Api.conn.get(getTypes);

			await new Promise((res) => {
				Db.realm.write(() => {
					const allTypes = Db.realm.objects('Type');
					Db.realm.delete(allTypes);

					data.forEach((attr) => {
						Db.realm.create('Type', {
							id: attr.id,
							description: attr.descricao,
							type: attr.type,
							code: attr.codigo,
						});
					});
					res();
				});
			});
		} catch (error) {
			tron.error('Erro ao popular tabela tipos');
		}
	}

	async populateAttributes() {
		try {
			const { data } = await Api.conn.get(getAttrs);

			await new Promise((res) => {
				Db.realm.write(() => {
					const allTypes = Db.realm.objects('Attribute');
					Db.realm.delete(allTypes);

					data.forEach((type) => {
						Db.realm.create('Attribute', {
							id: type.id,
							table: type.table,
							column: type.column,
							attr_type: type.tipo_atributo,
							obligatory: type.obrigatorio,
							referenced_table: type.tabela_referenciada,
							name: type.nome,
							order: type.ordem,
							show: JSON.stringify(type.exibir),
							edit: JSON.stringify(type.editar),
							required: JSON.stringify(type.obrigar),
						});
					});
					res();
				});
			});
		} catch (error) {
			tron.error('Erro ao popular tabela atributos');
			tron.error(error);
		}
	}

	async populateUsers() {
		try {
			const { data } = await Api.conn.get(getUsers, {
				params: {
					coletor: deviceName,
					// Mudar isso após montar uma forma de cadastrar um dispositivo no servidor
				},
			});

			await new Promise((res) => {
				Db.realm.write(() => {
					const allTypes = Db.realm.objects('User');
					Db.realm.delete(allTypes);

					data.forEach((user) => {
						Db.realm.create('User', {
							id: user.id,
							name: user.nome,
							login: user.login,
							password: user.senha,
						});
					});
					res();
				});
			});
		} catch (error) {
			tron.error('Erro ao popular tabela de usuários');
			tron.error(error);
		}
	}

	toToLogin() {
		const {
			navigation: { navigate },
		} = this.props;

		navigate('Login');
	}

	render() {
		const { host, finished } = this.state;
		return (
			<Container finished={finished}>
				{!finished ? (
					<>
						<Text>
							Precisamos que você nos informe o endereço do
							servidor com o qual você vai trabalhar, para que
							possamos começar.
						</Text>

						<Form>
							<Input
								placeHolder="Ex.: https://servidor.com.br/api"
								value={host}
								returnKeyType="send"
							/>
							<Button
								text="Conectar-se"
								marginTop="20"
								edita
								onPress={() => {
									this.handleConect();
								}}
							/>
						</Form>
					</>
				) : (
					<>
						<Text>
							O aplicativo foi configurado e está pronto para uso.
						</Text>
						<Button
							text="ir para a tela de login"
							marginTop="100"
							edita
							onPress={() => {
								this.toToLogin();
							}}
						/>
					</>
				)}
			</Container>
		);
	}
}
