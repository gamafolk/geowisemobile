import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';

export const Container = styled(LinearGradient).attrs({
	colors: ['#003967', '#0059a1'],
	start: { x: 0, y: 0 },
	end: { x: 1, y: 1 },
})`
	flex: 1;
	padding: 20px;
	justify-content: ${(p) => (p.finished ? 'flex-end' : 'space-evenly')};
`;

export const Text = styled.Text`
	width: 60%;
	font-size: 18px;
	text-align: left;
	color: #fff;
`;

export const Input = styled.TextInput.attrs({
	placeholderTextColor: '#999',
})`
	padding: 5px 15px;
	border-radius: 4px;
	font-size: 16px;
	color: #333;
	background: #fff;
	margin-bottom: 15px;
`;

export const Form = styled.View``;
