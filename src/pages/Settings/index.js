import React, { Component } from 'react';
import Storage from '@react-native-community/async-storage';

import { Container, List, ItemContainer, ItemTitle, ItemDesc } from './styles';

export default class Settings extends Component {
	state = { host: '' };

	async componentDidMount() {
		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Configurações',
		});

		const host = await Storage.getItem('host');

		this.setState({ host });
	}

	render() {
		const { host } = this.state;
		return (
			<Container>
				<List>
					<ItemContainer>
						<ItemTitle>Mudar servidor</ItemTitle>
						<ItemDesc>{host}</ItemDesc>
					</ItemContainer>
					<ItemContainer>
						<ItemTitle>Alterar senha</ItemTitle>
						<ItemDesc>Muda a senha do usuário atual</ItemDesc>
					</ItemContainer>
					<ItemContainer>
						<ItemTitle>Limpar banco de dados</ItemTitle>
						<ItemDesc>Deleta todos os dados salvos</ItemDesc>
					</ItemContainer>
					<ItemContainer>
						<ItemTitle>Resetar app</ItemTitle>
						<ItemDesc>
							Deleta o banco de dados e reinicia o app
						</ItemDesc>
					</ItemContainer>
					<ItemContainer>
						<ItemTitle>Fazer backup</ItemTitle>
						<ItemDesc>
							Faz backup dos dados salvos no banco de dados
						</ItemDesc>
					</ItemContainer>
					<ItemContainer>
						<ItemTitle>Sair</ItemTitle>
						<ItemDesc>Fazer logout</ItemDesc>
					</ItemContainer>
				</List>
			</Container>
		);
	}
}
