import styled from 'styled-components/native';

export const Container = styled.View`
	flex: 1;
	justify-content: center;
	align-items: center;
`;

export const List = styled.ScrollView`
	width: 100%;
	padding: 0 10px;
	margin-top: 10px;
`;

export const ItemContainer = styled.TouchableOpacity`
	align-items: flex-start;
	justify-content: center;
	padding: 10px;
	margin-bottom: 5px;
	background-color: #e8e8e8;
`;

export const ItemTitle = styled.Text`
	color: #37474f;
	font-size: 16px;
	font-weight: bold;
`;

export const ItemDesc = styled.Text`
	color: #018577;
	font-size: 11px;
`;
