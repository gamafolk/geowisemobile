import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Button from '../../Components/Button';

export const Container = styled.View`
	flex: 1;
	align-items: center;
`;

export const List = styled.FlatList.attrs({
	contentContainerStyle: {
		marginBottom: 10,
		padding: 30,
	},
})`
	flex: 1;
`;

export const BtnSync = styled(Button)`
	margin-top: 20px;
	margin-bottom: 20px;
`;

export const View = styled.View`
	border-bottom-color: #e0e0e0;
	border-bottom-width: 1px;
	padding-bottom: 5px;
	padding-top: 5px;
	flex-direction: row;
	align-items: center;
	justify-content: flex-start;
`;

export const CheckedIcon = styled(Icon).attrs({
	name: 'check',
	size: 20,
	color: '#009688',
})`
	margin-right: 10px;
`;

export const Text = styled.Text``;
