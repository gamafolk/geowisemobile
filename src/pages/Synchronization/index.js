/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import { Alert } from 'react-native';
import Storage from '@react-native-community/async-storage';

import Db from '../../services/Realm';
import Api from '../../services/Api';
import { Container, List, View, Text, BtnSync, CheckedIcon } from './styles';
import { loadDestinedRegisters } from '../../config/apiActions.json';
import { parseToDb } from '../../support/index';
import { deviceName } from '../../config/deviceName.json'; // Provisório

export default class Synchronization extends Component {
	state = {
		items: [],
	};

	componentDidMount() {
		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Sincronização',
		});
	}

	renderItem = ({ item }) => (
		<View>
			<CheckedIcon />
			<Text>{item.message}</Text>
		</View>
	);

	async confirm() {
		return new Promise((res) => {
			Alert.alert(
				'Iniciar sincronização',
				'tem certeza que deseja inicar a sincronização?',
				[
					{
						text: 'Sincronizar',
						onPress: () => res(true),
					},
					{
						text: 'Cancelar',
						onPress: () => res(false),
					},
				]
			);
		});
	}

	async syncData() {
		if (!(await this.confirm())) return;
		const user = JSON.parse(await Storage.getItem('logged-user'));
		try {
			const { data } = await Api.conn.get(loadDestinedRegisters, {
				params: {
					coletor: deviceName,
					usuario: {
						id: user.id,
						nome: user.name,
						login: user.login,
						senha: user.password,
						sync: user.sync,
					},
				},
			});

			await this.populateRegisterTable(data.cadastros_destinados);
			await this.populateAllTables(data);

			const { navigation } = this.props;
			navigation.navigate('MainRoutes');
		} catch (error) {
			tron.log(error);
		}
	}

	async populateRegisterTable(data) {
		await new Promise((res) => {
			Db.realm.write(() => {
				const allTypes = Db.realm.objects('Register');
				Db.realm.delete(allTypes); // Provisório

				data.forEach((register) => {
					Db.realm.create('Register', {
						id: register.id,
						inscription: register.inscricao,
						relationship_id: register.relacionamento_id,
						process_status: register.status_processo,
						survey_status: register.status_levantamento,
						scheduling_date: register.data_agendamento,
						visits_number: register.quantidade_visitas,
						register_type: register.tipo_cadastro,
						type: register.type,
						property_point: register.ponto_imovel,
						period: register.periodo,
						contact: register.contato,
						info: '',
						visits_made: 0,
					});
				});
				res();
			});
		});
	}

	async populateAllTables(data) {
		const schema = JSON.parse(await Storage.getItem('schema'));
		try {
			Db.realm.write(() => {
				Object.keys(data).forEach((key) => {
					// Fazer com que essa verificação seja mais segura trazendo o nome da tabela em questão do backend
					const [model] = schema.filter(
						(e) =>
							key.toUpperCase().includes(e.name.toUpperCase()) &&
							!key.toUpperCase().includes('IMAGE')
					);

					if (!model) return;

					// Provisósrio
					Db.realm.delete(Db.realm.objects(model.name));

					data[key].forEach((obj) => {
						Db.realm.create(model.name, parseToDb(obj, model));
					});
				});
			});
		} catch (error) {
			tron.error(error);
			console.log(error);
		}
	}

	render() {
		const { items } = this.state;
		return (
			<Container>
				<List
					data={items}
					renderItem={this.renderItem}
					keyExtractor={(item) => item.id}
				/>
				<BtnSync text="Sincronizar" onPress={() => this.syncData()} />
			</Container>
		);
	}
}
