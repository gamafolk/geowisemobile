import React, { Component } from 'react';

import {
	MapView,
	MenuIcon,
	// CreateContainer,
	// CreateView,
	// CreateText,
	// Modal,
} from './styles';
import MenuPopUp from '../../Components/MenuPopUp';

export default class Map extends Component {
	componentDidMount() {
		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Mapa',
			headerRight: () => (
				<MenuPopUp
					items={[
						{
							label: 'Adicionar territorial',
							icon: () => <MenuIcon name="add" />,
							action: () => {
								tron.log('Adicionar territorial');
							},
						},
					]}
				/>
			),
		});
	}

	render() {
		return (
			<>
				<MapView />
				{/* <Modal visible>
					<CreateContainer>
						<CreateView>
							<CreateText>
								Toque um ponto do mapa para escolher a
								localização do territorial que vai criar.
							</CreateText>
						</CreateView>
					</CreateContainer>
				</Modal> */}
			</>
		);
	}
}
