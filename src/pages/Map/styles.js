import styled from 'styled-components/native';
import MapboxGl from '@react-native-mapbox-gl/maps';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { accessToken } from '../../config/map.json';

MapboxGl.setAccessToken(accessToken);
const { MapView: Map } = MapboxGl;

export const MapView = styled(Map)`
	flex: 1;
`;

export const MenuIcon = styled(Icon).attrs({
	color: '#018577',
	size: 20,
})``;

export const CreateContainer = styled.View`
	background-color: transparent;
	flex: 1;
	justify-content: flex-end;
	align-items: center;
`;

export const CreateView = styled.View`
	background-color: #fff;
	margin-bottom: 60px;
	padding: 10px 20px;
	border-radius: 10px;
	width: 90%;
	justify-content: center;
	align-items: center;
	text-align: center;
	opacity: 0.8;
`;

export const CreateText = styled.Text`
	color: #37474f;
	justify-content: center;
	align-items: center;
	text-align: center;
`;

export const Modal = styled.Modal.attrs({
	animationType: 'none',
	transparent: true,
})`
	background: red;
`;
