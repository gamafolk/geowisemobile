import React, { Component } from 'react';
import bcrypt from 'react-native-bcrypt';
import Storage from '@react-native-community/async-storage';
import Db from '../../services/Realm';

import {
	Container,
	Form,
	BackGround,
	InputCont,
	Input,
	Submit,
	ButtonText,
	LogoContainer,
	LogoImage,
	Text,
	PersonIcon,
	LockIcon,
	Button,
	TextLogo,
	ErrorText,
} from './styles';

export default class Login extends Component {
	state = {
		login: 'biancachu',
		password: '1234567890',
		passwordError: '',
		logindError: '',
	};

	async handleLogin() {
		const { login, password } = this.state;
		const [user] = Db.realm
			.objects('User')
			.filtered(`login = "${login.trim()}"`);

		if (user) {
			if (!(await bcrypt.compareSync(password.trim(), user.password))) {
				this.setState({ passwordError: 'Senha inválida' });
			} else {
				await Storage.setItem('logged-user', JSON.stringify(user));

				const { navigation } = this.props;
				navigation.navigate('MainRoutes');
			}
		} else {
			this.setState({ logindError: 'Nome de usuário inválido' });
		}
	}

	render() {
		const { login, password, passwordError, logindError } = this.state;
		return (
			<Container>
				<BackGround>
					<Form>
						<LogoContainer>
							<LogoImage />
							<TextLogo>GEOWISE</TextLogo>
						</LogoContainer>
						<InputCont error={logindError}>
							<PersonIcon error={logindError} />
							<Input
								placeholder="Nome de usuário"
								onChangeText={(text) =>
									this.setState({
										login: text,
										passwordError: '',
										logindError: '',
									})
								}
								value={login}
							/>
						</InputCont>
						<InputCont error={passwordError}>
							<LockIcon error={passwordError} />
							<Input
								placeholder="Senha"
								secureTextEntry
								returnKeyType="send"
								returnKeyLabel="Entrar"
								onSubmitEditing={() => this.handleLogin()}
								onChangeText={(text) =>
									this.setState({
										password: text,
										passwordError: '',
										logindError: '',
									})
								}
								value={password}
							/>
						</InputCont>

						<ErrorText show={passwordError || logindError}>
							{logindError || passwordError}
						</ErrorText>

						<Submit onPress={() => this.handleLogin()}>
							<ButtonText>Entrar</ButtonText>
						</Submit>

						<Button>
							<Text>REGISTRAR APP</Text>
						</Button>
					</Form>
				</BackGround>
			</Container>
		);
	}
}
