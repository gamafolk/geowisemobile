import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import bgImage from '../../img/login_bg.jpg';
import logoImage from '../../img/logo.png';

export const Container = styled.View`
	flex: 1;
	justify-content: center;
	align-items: center;
`;

export const LogoContainer = styled.View`
	margin-bottom: 30px;
	padding: 20px;
`;

export const LogoImage = styled.Image.attrs({
	source: logoImage,
})`
	height: 100px;
	width: 200px;
`;

export const BackGround = styled.ImageBackground.attrs({
	source: bgImage,
})`
	flex: 1;
	width: 100%;
	resize-mode: cover;
	justify-content: center;
	align-items: center;
`;

export const Form = styled.View`
	flex: 1;
	width: 100%;
	background-color: rgba(0, 57, 103, 0.5);
	justify-content: center;
	align-items: center;
`;

export const TextLogo = styled.Text`
	color: #fff;
	text-align: center;
	letter-spacing: 5px;
	font-size: 15px;
`;

export const Text = styled.Text`
	color: #fff;
	text-align: center;
	font-size: 15px;
	border-bottom-width: 1px;
	border-bottom-color: #fff;
	font-weight: 900;
`;

export const ErrorText = styled.Text`
	color: ${(p) => (p.show.trim() ? '#fff' : 'transparent')};
	margin-top: 15px;
	text-align: center;
	font-size: 10px;
	font-weight: 900;
`;

export const ButtonText = styled.Text`
	color: #808080;
	font-weight: 900;
	font-size: 16px;
`;

export const InputCont = styled.View`
	border-bottom-color: ${(p) => (p.error ? '#f44336' : '#fff')};
	border-bottom-width: 1px;
	margin-top: 20px;
	width: 80%;
	flex-direction: row;
	align-items: center;
	padding: 5px;
`;

export const Input = styled.TextInput.attrs({
	placeholderTextColor: 'rgba(238, 238, 238, 0.7)',
	autoCorrect: false,
	autoCapitalize: 'none',
})`
	flex: 1;
	color: #fff;
	font-size: 17px;
	padding: 0;
	margin-left: 20px;
	text-align: left;
`;

export const Submit = styled.TouchableOpacity`
	font-size: 17px;
	width: 200px;
	/* background-color: rgba(238, 238, 238, 0.3); */
	background-color: #ffffffde;
	margin-top: 15px;
	padding: 10px 20px;
	justify-content: center;
	align-items: center;
	border-radius: 7px;
`;

export const Button = styled.TouchableOpacity`
	font-size: 17px;
	margin-top: 10px;
	padding: 10px 20px;
	justify-content: center;
	align-items: center;
	border-radius: 7px;
`;

export const PersonIcon = styled(Icon).attrs({
	name: 'person',
	size: 27,
})`
	color: ${(p) => (p.error ? '#ef5350 ' : '#fff')};
`;

export const LockIcon = styled(Icon).attrs({
	name: 'lock',
	size: 25,
})`
	color: ${(p) => (p.error ? '#ef5350 ' : '#fff')};
`;
