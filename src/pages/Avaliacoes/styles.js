import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { CheckBox as Check } from 'react-native';

export const Container = styled.View`
	flex: 1;
	align-items: center;
`;

export const ItemText = styled.Text`
	font-family: 'Arial, sans-serif';
	color: #37474f;
`;

export const ItemTile = styled.Text`
	font-family: 'Arial, sans-serif';
	font-size: 15px;
	font-weight: bold;
	color: #37474f;
`;

export const AutonomaView = styled.TouchableOpacity`
	align-items: flex-start;
	justify-content: center;
	padding: 10px;
	background-color: #e8e8e8;
	width: 100%;
	margin-bottom: 10px;
`;

export const AutonomaContainer = styled.View`
	padding: 10px 10px 0px 10px;
	width: 100%;
	border-bottom-width: 1px;
	border-bottom-color: 1px;
`;

export const Sep = styled.View`
	background-color: #e0e0e0;
	width: 100%;
	height: 2px;
`;

export const MenuIcon = styled(Icon).attrs({
	color: '#018577',
	size: 20,
})``;

export const List = styled.ScrollView`
	width: 100%;
	padding: 0 10px;
	margin-top: 10px;
`;

export const View = styled.TouchableOpacity`
	flex-direction: row;
	align-items: center;
	justify-content: flex-start;
	padding: 10px 10px 10px ${(p) => (p.selectionMode ? '0' : '10px')};
	margin-bottom: 5px;
	background-color: #e8e8e8;
`;

export const Checkbox = styled(Check)`
	margin-right: 10px;
`;

export const SmallContainer = styled.View``;

export const Text = styled.Text`
	font-family: 'Arial, sans-serif';
	color: #37474f;
`;
