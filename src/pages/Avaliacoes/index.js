import React, { Component } from 'react';

import {
	Container,
	ItemText,
	ItemTile,
	AutonomaContainer,
	AutonomaView,
	Sep,
	MenuIcon,
	List,
	View,
	Checkbox,
	SmallContainer,
	Text,
} from './styles';
import Db from '../../services/Realm';

import MenuPopUp from '../../Components/MenuPopUp';

export default class Avaliacoes extends Component {
	state = { autonoma: {}, items: [], selectionMode: false };

	componentDidMount() {
		const { route } = this.props;
		this.setState({
			autonoma:
				Db.realm
					.objects('autonomas')
					.filtered(`id = ${route.params.autonomaId}`)[0] || {},
			items: Db.realm
				.objects('avaliacoes')
				.filtered(`autonoma_id = ${route.params.autonomaId}`),
		});
		this.setmenu();
	}

	setmenu() {
		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Avaliações',
			headerRight: () => (
				<MenuPopUp
					items={[
						{
							label: 'Selecionar avaliações',
							icon: () => <MenuIcon name="check" />,
							action: () => {
								this.setSelectionMode();
							},
						},
						{
							label: 'Adicionar avaliações',
							icon: () => <MenuIcon name="add" />,
							action: () => {
								tron.log('Adicionar avaliações');
							},
						},
						{
							label: 'Remover avaliações',
							icon: () => <MenuIcon name="delete" />,
							action: () => {
								tron.log('Remover avaliações');
							},
						},
						{
							label: 'Informações da autônoma',
							icon: () => <MenuIcon name="info" />,
							action: () => {
								tron.log('Informações da autônoma');
							},
						},
					]}
				/>
			),
		});
	}

	setSelectionMode() {
		const { selectionMode, items } = this.state;
		this.setState({
			selectionMode: !selectionMode,
			items: items.map((e) => {
				e.selected = false;
				return e;
			}),
		});
	}

	itemPressHandle(index) {
		const { items, selectionMode } = this.state;

		if (selectionMode) {
			this.setState({
				items: items.map((e, i) => {
					if (i === index) e.selected = !e.selected;
					return e;
				}),
			});
		} else {
			const { navigation } = this.props;
			navigation.navigate('Edition', {
				table: 'avaliacoes',
				id: items[index].id,
			});
		}
	}

	openEditPage(params) {
		const { navigation } = this.props;
		navigation.navigate('Edition', params);
	}

	render() {
		const { autonoma, items, selectionMode } = this.state;
		return (
			<Container>
				<AutonomaContainer>
					<AutonomaView
						onPress={() =>
							this.openEditPage({
								table: 'autonomas',
								id: autonoma.id,
							})
						}
					>
						<ItemTile>Autonoma</ItemTile>
						<ItemText>{autonoma.inscricao}</ItemText>
					</AutonomaView>
					<Sep />
				</AutonomaContainer>
				<List>
					{items.map((item, i) => (
						<View
							key={String(i)}
							selectionMode={selectionMode}
							onPress={() => {
								this.itemPressHandle(i);
							}}
						>
							{selectionMode && (
								<Checkbox value={item.selected} />
							)}
							<SmallContainer>
								<ItemTile>Avaliação</ItemTile>
								<Text>{item.inscricao}</Text>
							</SmallContainer>
						</View>
					))}
				</List>
			</Container>
		);
	}
}
