import React, { Component } from 'react';

import {
	Container,
	ItemText,
	ItemTile,
	TerritorialView,
	TerritorialContainer,
	Sep,
	MenuIcon,
	View,
	List,
	Checkbox,
	SmallContainer,
	Text,
} from './styles';
import Db from '../../services/Realm';

import MenuPopUp from '../../Components/MenuPopUp';

export default class Autonomas extends Component {
	state = {
		territorial: {},
		items: [],
		selectionMode: false,
	};

	componentDidMount() {
		const { route } = this.props;
		this.setState({
			territorial:
				Db.realm
					.objects('territoriais')
					.filtered(`id = ${route.params.territorialId}`)[0] || {},
			items: Db.realm
				.objects('autonomas')
				.filtered(`territorial_id = ${route.params.territorialId}`),
		});

		this.setmenu();
	}

	setmenu() {
		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Autonomas',
			headerRight: () => (
				<MenuPopUp
					items={[
						{
							label: 'Selecionar autônomas',
							icon: () => <MenuIcon name="check" />,
							action: () => {
								this.setSelectionMode();
							},
						},
						{
							label: 'Adicionar autônomas',
							icon: () => <MenuIcon name="add" />,
							action: () => {
								navigation.navigate('Map', {
									fromOutside: true,
								});
							},
						},
						{
							label: 'Remover autônomas',
							icon: () => <MenuIcon name="delete" />,
							action: () => {
								tron.log('Remover autonomas');
							},
						},
					]}
				/>
			),
		});
	}

	setSelectionMode() {
		const { selectionMode, items } = this.state;
		this.setState({
			selectionMode: !selectionMode,
			items: items.map((e) => {
				e.selected = false;
				return e;
			}),
		});
	}

	openEditPage() {
		const { navigation } = this.props;
		const { territorial } = this.state;

		navigation.navigate('Edition', {
			id: territorial.id,
			table: 'territoriais',
		});
	}

	itemPressHandle(index) {
		const { items, selectionMode } = this.state;

		if (selectionMode) {
			this.setState({
				items: items.map((e, i) => {
					if (i === index) e.selected = !e.selected;
					return e;
				}),
			});
		} else {
			const { navigation } = this.props;
			navigation.navigate('Avaliacoes', {
				autonomaId: items[index].id,
			});
		}
	}

	render() {
		const { territorial, items, selectionMode } = this.state;
		return (
			<Container>
				<TerritorialContainer>
					<TerritorialView onPress={() => this.openEditPage()}>
						<ItemTile>Territorial</ItemTile>
						<ItemText>{territorial.inscricao}</ItemText>
					</TerritorialView>
					<Sep />
				</TerritorialContainer>
				<List>
					{items.map((item, i) => (
						<View
							key={String(i)}
							selectionMode={selectionMode}
							onPress={() => this.itemPressHandle(i)}
						>
							{selectionMode && (
								<Checkbox value={item.selected} />
							)}
							<SmallContainer>
								<ItemTile>Autonoma</ItemTile>
								<Text>{item.inscricao}</Text>
							</SmallContainer>
						</View>
					))}
				</List>
			</Container>
		);
	}
}
