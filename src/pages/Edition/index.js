/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import { Container, List, ListContainer } from './styles';

import Input from '../../Components/Input';
// import ListView from '../../Components/ListView';

import Db from '../../services/Realm';

export default class Edition extends Component {
	state = {
		inputs: [],
	};

	componentDidMount() {
		const { route } = this.props;

		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Edição',
		});

		const attrs = Db.realm
			.objects('Attribute')
			.filtered(`table = "${route.params.table}"`);

		const inputs = [];

		const [o] = Db.realm
			.objects(route.params.table)
			.filtered(`id = ${route.params.id}`);

		const object = JSON.parse(JSON.stringify(o));

		Object.keys(object).forEach((key, i) => {
			const [attr] = attrs.filter((e) => e.column === key);
			if (!attr) return;
			// options: ['Proprietario', 'Caseiro'],
			let options = [];
			if (attr.attr_type.toUpperCase().trim() === 'OPCOES') {
				options = Db.realm
					.objects('Type')
					.filtered(`type = "${this.assignType(attr.column)}"`);
			}

			inputs.push({
				type: attr.attr_type,
				value: object[key],
				label: attr.name,
				id: i,
				options,
				key,
			});
		});
		this.setState({ inputs });
	}

	assignType(column) {
		return `Geowise::${column
			.replace('_id', '')
			.replace(/[_]/g, ' ')
			.replace(/(?:^|\s)\S/g, (a) => a.toUpperCase())
			.replace(/[ ]/g, '')}`;
	}

	render() {
		const { inputs } = this.state;
		return (
			<Container>
				<List>
					<ListContainer>
						{inputs.map((item, i) => {
							return (
								<Input
									key={item.id}
									config={{
										type: item.type,
										label: item.label,
										value: item.value,
										options: item.options,
										onChange: (value) => {
											inputs[i].value = value;
											this.setState({ inputs });
										},
									}}
								/>
							);
						})}
					</ListContainer>
				</List>
			</Container>
		);
	}
}
