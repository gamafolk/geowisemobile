import styled from 'styled-components/native';
import { CheckBox as Check } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const Container = styled.View`
	flex: 1;
	align-items: center;
`;

export const ListContainer = styled.View`
	padding: 7px;
	width: 100%;
`;

export const List = styled.ScrollView``;

export const View = styled.TouchableOpacity`
	flex-direction: row;
	align-items: center;
	justify-content: flex-start;
	padding: 10px 10px 10px ${(p) => (p.selectionMode ? '0' : '10px')};
	margin-bottom: 5px;
	background-color: #e8e8e8;
`;

export const Checkbox = styled(Check)`
	margin-right: 10px;
`;

export const ItemTile = styled.Text`
	font-family: 'Arial, sans-serif';
	font-size: 15px;
	font-weight: bold;
	color: #37474f;
`;

export const SmallContainer = styled.View``;

export const Text = styled.Text`
	font-family: 'Arial, sans-serif';
	color: #37474f;
`;

export const MenuIcon = styled(Icon).attrs({
	color: '#018577',
	size: 20,
})``;
