import React, { Component } from 'react';

import MenuPopUp from '../../Components/MenuPopUp';

import Db from '../../services/Realm';
import {
	Container,
	ListContainer,
	List,
	View,
	ItemTile,
	Text,
	MenuIcon,
	Checkbox,
	SmallContainer,
} from './styles';

export default class Territoriais extends Component {
	state = { items: [], selectionMode: false };

	async componentDidMount() {
		this.setState({
			items: Db.realm.objects('territoriais'),
		});

		const { navigation } = this.props;
		navigation.setOptions({
			title: 'Territoriais',
			headerRight: () => {
				return (
					<MenuPopUp
						items={[
							{
								label: 'Adicionar territorial',
								icon: () => <MenuIcon name="add" />,
								action: () => {
									navigation.navigate('Map', {
										fromOutside: true,
									});
								},
							},
						]}
					/>
				);
			},
		});
	}

	render() {
		const { items, selectionMode } = this.state;
		return (
			<Container>
				<ListContainer>
					<List>
						{items.map((item, i) => (
							<View
								key={String(i)}
								selectionMode={selectionMode}
								onPress={() => {
									const { navigation } = this.props;
									navigation.navigate('Autonomas', {
										territorialId: item.id,
									});
								}}
							>
								{selectionMode && (
									<Checkbox value={item.selected} />
								)}
								<SmallContainer>
									<ItemTile>Territorial</ItemTile>
									<Text>{item.inscricao}</Text>
								</SmallContainer>
							</View>
						))}
					</List>
				</ListContainer>
			</Container>
		);
	}
}
