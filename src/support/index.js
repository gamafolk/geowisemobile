/* eslint-disable no-restricted-globals */
export const isNull = (value) => {
	const v = typeof value === 'string' ? value.toUpperCase() : value;
	return (
		v === null ||
		v === undefined ||
		(typeof v === 'string' && v === 'NULL') ||
		(typeof v === 'string' && v === 'UNDEFINED')
	);
};

export const parseToDb = (d, m) => {
	const data = d || {};
	const model = m || {};

	const newData = {};
	const { properties } = model;

	Object.keys(properties).forEach((key) => {
		let value = data[key];
		switch (properties[key].type) {
			case 'string':
				value =
					typeof value === 'object' ? JSON.stringify(value) : value;
				value = isNull(value) ? null : String(value);
				break;
			case 'int':
				value = parseInt(value, 10);
				value = isNaN(value) ? null : value;
				break;
			case 'float':
				value = parseFloat(value);
				value = isNaN(value) ? null : value;
				break;
			default:
				break;
		}
		newData[key] = value;
	});

	return newData;
};
