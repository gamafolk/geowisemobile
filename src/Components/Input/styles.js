import styled from 'styled-components/native';
import { Picker, CheckBox as Check } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export const Container = styled.View`
	margin-bottom: 15px;
`;

export const Label = styled.Text`
	font-size: 14px;
	color: #37474f;
	margin-bottom: 2px;
`;

export const TextInput = styled.TextInput.attrs({
	placeholderTextColor: '#999',
})`
	padding: 3px 10px;
	border-radius: 3px;
	font-size: 15px;
	color: #37474f;
	background: #fff;
	border-width: 1px;
	border-color: ${(p) => (p.focused ? '#018577' : '#e0e0e0')};
`;

export const SelectContainer = styled.TouchableOpacity`
	border-radius: 3px;
	background: #fff;
	border-width: 1px;
	border-color: #e0e0e0;
	height: 37px;
	justify-content: center;
`;

export const Select = styled(Picker)`
	font-size: 15px;
	height: 40px;
	color: #37474f;
`;

export const Option = styled(Picker.Item)``;

export const CheckBox = styled(Check)``;

export const CheckContainer = styled.View`
	flex-direction: row;
	align-items: center;
`;

export const CheckLabel = styled.Text`
	font-size: 15px;
	color: #37474f;
	margin-left: 5px;
	flex: 1;
`;

export const Date = styled.TouchableOpacity`
	padding: 3px 10px;
	border-radius: 3px;
	font-size: 15px;
	color: #37474f;
	background: #fff;
	border-width: 1px;
	border-color: #e0e0e0;
	height: 37px;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;
`;

export const DateValue = styled.Text`
	font-size: 15px;
	color: #37474f;
	margin-left: 5px;
	flex: 1;
`;

export const DateIcon = styled(Icon).attrs({
	name: 'ios-calendar',
	color: '#37474f',
	size: 20,
})``;

export const RadioButtonContainer = styled.View`
	flex-direction: row;
	width: 100%;
	justify-content: flex-start;
	align-items: flex-start;
`;

export const RadioButton = styled.TouchableOpacity`
	flex-direction: row;
`;

export const RadioLabel = styled.Text`
	font-size: 15px;
	color: #37474f;
	margin-left: 5px;
	width: 70px;
`;

export const RadioButtonOn = styled(Icon).attrs({
	name: 'md-radio-button-on',
	color: '#018577',
	size: 23,
})``;

export const RadioButtonOff = styled(Icon).attrs({
	name: 'md-radio-button-off',
	color: '#018577',
	size: 23,
})``;
