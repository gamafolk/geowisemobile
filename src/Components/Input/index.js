/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import { DatePickerAndroid as DatePicker } from 'react-native';

import {
	Container,
	Label,
	TextInput,
	Select,
	Option,
	SelectContainer,
	CheckBox,
	CheckContainer,
	CheckLabel,
	Date,
	DateValue,
	DateIcon,
	RadioButton,
	RadioButtonOn,
	RadioButtonOff,
	RadioButtonContainer,
	RadioLabel,
} from './styles';

export default class Input extends Component {
	state = { focused: false };

	async getDate() {
		const { day, month, year } = await DatePicker.open();
		if (!day || !month || !year) return undefined;
		return `${day}/${month}/${year}`;
	}

	getOptions() {}

	cbxParse(value) {
		switch (String(value).toUpperCase().trim()) {
			case 'ON':
				return true;
			case 'TRUE':
				return true;
			case 'CHECKED':
				return true;
			case '1':
				return true;
			case 'OFF':
				return false;
			case 'FALSE':
				return false;
			case '0':
				return false;
			default:
				return false;
		}
	}

	render() {
		const { config, style } = this.props;

		const { focused } = this.state;

		const {
			type = '',
			label = '',
			value,
			onChange = () => {},
			options = [],
		} = config;

		return (
			<Container style={style}>
				{type.toUpperCase() !== 'BOOLEAN' && <Label>{label}</Label>}

				{(() => {
					switch (type.toUpperCase()) {
						case 'STRING':
							return (
								<TextInput
									value={value}
									onChangeText={onChange}
									focused={focused}
									onFocus={() =>
										this.setState({ focused: true })
									}
									onBlur={() =>
										this.setState({ focused: false })
									}
								/>
							);

						case 'FLOAT':
						case 'BIGDECIMAL':
						case 'INTEGER':
							return (
								<TextInput
									focused={focused}
									onFocus={() =>
										this.setState({ focused: true })
									}
									onBlur={() =>
										this.setState({ focused: false })
									}
									value={
										value
											? String(value).replace(
													/[^0-9.,]/g,
													''
											  )
											: ''
									}
									keyboardType="numeric"
									onChangeText={onChange}
								/>
							);

						case 'BOOLEAN':
							return (
								<CheckContainer>
									<CheckBox
										value={this.cbxParse(value)}
										onValueChange={onChange}
									/>
									<CheckLabel
										onPress={() => {
											onChange(!value);
										}}
									>
										{label}
									</CheckLabel>
								</CheckContainer>
							);

						case 'OPCOES':
							return (
								<SelectContainer>
									<Select
										selectedValue={value}
										onValueChange={onChange}
									>
										{options.map((e) => {
											return (
												<Option
													label={e.description}
													value={e.id}
												/>
											);
										})}
									</Select>
								</SelectContainer>
							);

						case 'TIME':
						case 'DATE':
						case 'DATETIME':
							return (
								<Date
									onPress={async () => {
										const date = await this.getDate();
										if (date) onChange(date);
									}}
								>
									<DateValue>{value}</DateValue>
									<DateIcon />
								</Date>
							);

						case 'FOREINGKEY':
							return (
								<RadioButtonContainer>
									<RadioButton
										onPress={() => {
											onChange(true);
										}}
									>
										{value ? (
											<RadioButtonOn />
										) : (
											<RadioButtonOff />
										)}
										<RadioLabel>Sim</RadioLabel>
									</RadioButton>
									<RadioButton
										onPress={() => {
											onChange(false);
										}}
									>
										{!value ? (
											<RadioButtonOn />
										) : (
											<RadioButtonOff />
										)}
										<RadioLabel>Não</RadioLabel>
									</RadioButton>
								</RadioButtonContainer>
							);

						default:
							return (
								<TextInput
									value={value}
									onChangeText={onChange}
									focused={focused}
									onFocus={() =>
										this.setState({ focused: true })
									}
									onBlur={() =>
										this.setState({ focused: false })
									}
								/>
							);
					}
				})()}
			</Container>
		);
	}
}
