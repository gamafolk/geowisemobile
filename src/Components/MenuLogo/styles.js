import styled from 'styled-components/native';
import logo from '../../img/logo.png';

export const Image = styled.Image.attrs({
	source: logo,
})`
	width: 41px;
	height: 20px;
`;
