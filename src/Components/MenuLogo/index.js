import React from 'react';
import { Shadow as Container } from 'react-native-neomorph-shadows';
import { Image } from './styles';

export default function MenuLogo(props) {
	const { focused } = props;
	return (
		<Container
			inner
			useArt
			style={{
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0.6,
				shadowColor: focused ? '#003967' : 'gray',
				shadowRadius: 6,
				borderRadius: 22,
				backgroundColor: '#fff',
				width: 44,
				height: 44,
				marginBottom: 12,
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			<Image />
		</Container>
	);
}
