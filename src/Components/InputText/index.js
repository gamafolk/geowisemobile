import styled from 'styled-components/native';

const Input = styled.TextInput.attrs({
	placeholderTextColor: '#999',
})`
	padding: 5px 15px;
	border-radius: 4px;
	font-size: 16px;
	color: #37474f;
	background: #fff;
`;

export default Input;
