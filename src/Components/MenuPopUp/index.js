import React, { Component } from 'react';

import {
	Container,
	MoreIcon,
	IconContainer,
	Modal,
	MenuText,
	Touch,
	MenuContainer,
	MenuItem,
	Cont,
	ItemIconCont,
} from './styles';

export default class MenuPopUp extends Component {
	state = { items: [], visible: false };

	componentDidMount() {
		const { items = [] } = this.props;

		this.setState({ items });
	}

	render() {
		const { visible, items } = this.state;

		return (
			<Container>
				<IconContainer
					onPress={() => this.setState({ visible: !visible })}
				>
					<MoreIcon />
				</IconContainer>
				<Modal visible={visible}>
					<Touch onPress={() => this.setState({ visible: !visible })}>
						<Cont>
							<MenuContainer>
								{items.map((e, i) => (
									<MenuItem
										disabled={e.disabled}
										key={String(i)}
										onPress={() => {
											this.setState({
												visible: false,
											});
											if (e.action) e.action();
										}}
									>
										<ItemIconCont>
											{e.icon && e.icon()}
										</ItemIconCont>

										<MenuText>{e.label}</MenuText>
									</MenuItem>
								))}
							</MenuContainer>
						</Cont>
					</Touch>
				</Modal>
			</Container>
		);
	}
}
