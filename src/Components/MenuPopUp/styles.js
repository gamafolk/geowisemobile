import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/Ionicons';

export const Container = styled.View`
	width: 50px;
	flex: 1;
	align-items: center;
`;

export const Touch = styled.TouchableWithoutFeedback``;

export const Cont = styled.View`
	flex: 1;
	justify-content: flex-start;
	align-items: flex-end;
`;

export const MenuContainer = styled.View`
	background-color: #fff;
	padding: 8px 0;
	border-color: #e8e8e8;
	border-width: 1px;
`;

export const ItemIconCont = styled.View`
	width: 30px;
	align-items: center;
`;

export const MenuItem = styled.TouchableOpacity`
	padding: 6px 15px 6px 0px;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
	${(p) => p.disabled && 'opacity: .4;'}
`;

export const MenuText = styled.Text`
	font-size: 17px;
	color: #37474f;
	margin-left: 5px;
`;

export const IconContainer = styled.TouchableOpacity`
	flex: 1;
	width: 100%;
	justify-content: center;
	align-items: flex-end;
`;

export const MoreIcon = styled(Icon).attrs({
	name: 'md-more',
	size: 25,
})`
	margin-right: 20px;
`;

export const Modal = styled.Modal.attrs({
	animationType: 'none',
	transparent: true,
})``;
