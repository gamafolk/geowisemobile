import styled, { css } from 'styled-components/native';

export const Container = styled.TouchableOpacity`
	padding: 5px 15px;
	border-radius: 4px;
	background: #003967;
	justify-content: center;
	align-items: center;
	align-self: ${(p) => p.position || 'center'};
	${(p) =>
		p.block &&
		css`
			width: 100%;
		`}

	${(p) =>
		p.marginTop &&
		css`
			margin-top: ${p.marginTop}px;
		`}
`;

export const Text = styled.Text`
	font-size: 16px;
	text-align: left;
	justify-content: center;
	align-items: center;
	color: #fff;
`;
