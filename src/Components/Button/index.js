import React from 'react';

import { Container, Text } from './styles';

export default function Button(props) {
	const {
		text = '',
		block,
		style = {},
		textStyle = {},
		position,
		onPress = () => {},
	} = props;

	return (
		<Container
			block={block}
			style={style}
			position={position}
			onPress={onPress}
		>
			<Text style={textStyle}>{text}</Text>
		</Container>
	);
}
