import styled from 'styled-components/native';

const List = styled.FlatList.attrs({
	contentContainerStyle: {
		padding: 10,
	},
})`
	flex: 1;
	width: 100%;
`;

export default List;
